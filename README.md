# GitLab CI template for Puppeteer

This project implements a GitLab CI/CD template to run your automated (web) tests with [Puppeteer](https://pptr.dev/).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: $CI_SERVER_FQDN/to-be-continuous/puppeteer/gitlab-ci-puppeteer@3.7.0
    # 2: set/override component inputs
    inputs:
      # ⚠ this is only an example
      project-dir: "e2e"
      review-enabled: "true"
```

### Use as a CI/CD template (legacy)

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/puppeteer'
    ref: '3.7.0'
    file: '/templates/gitlab-ci-puppeteer.yml'

variables:
  # 2: set/override template variables
  # ⚠ this is only an example
  PUPPETEER_PROJECT_DIR: "e2e"
  REVIEW_ENABLED: "true"
```

## `puppeteer` job

This job starts [Puppeteer](https://pptr.dev/) (functional) tests.

It uses the following variable:

| Input / Variable | Description                              | Default value     |
| --------------------- | ---------------------------------------- | ----------------- |
| `image` / `PUPPETEER_IMAGE` | The Docker image used to run Puppeteer   | `ghcr.io/puppeteer/puppeteer:latest` <br/>[![Trivy Badge](https://to-be-continuous.gitlab.io/doc/secu/trivy-badge-PUPPETEER_IMAGE.svg)](https://to-be-continuous.gitlab.io/doc/secu/trivy-PUPPETEER_IMAGE) |
| `project-dir` / `PUPPETEER_PROJECT_DIR` | The Puppeteer project directory (containing `package.json`) | `.` |
| `test-extra-args` / `PUPPETEER_TEST_EXTRA_ARGS` | Testing framework extra options based on [Jest](https://jestjs.io/docs/en/cli) | _none_ |
| `review-enabled` / `REVIEW_ENABLED` | Set to `true` to enable Puppeteer tests on review environments (dynamic environments instantiated on development branches) | _none_ (disabled) |

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `$PUPPETEER_PROJECT_DIR/reports/puppeteer.xunit.xml` | [xUnit](https://en.wikipedia.org/wiki/XUnit) test report(s) | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsjunit) |

### Jest

Here we use [Jest](https://jestjs.io/) as testing framework and obviously choose [jest-junit](https://www.npmjs.com/package/jest-junit) to generate Junit test reports

Add the package as a development dependency:

```bash
npm install --save-dev jest jest-junit
```

and update `package.json` to add scripts section to invoke jest command as below:

```js
"scripts": {
  "puppeteer": "jest"
}
```

:warning: In your puppeteer scripts, take all screenshots under `puppeteer/screenshots`.

By default, reports are generated with file name `TEST-REPORT.xml` under `puppeteer/reports` directory. You can override these values by using variables `JEST_JUNIT_OUTPUT_DIR` and `JEST_JUNIT_OUTPUT_NAME` accordingly.

### Puppeteer `baseUrl` auto evaluation

By default, the Puppeteer template auto-evaluates a [baseUrl setting with Jest](https://jestjs.io/docs/en/configuration#globals-object)
(i.e. the variable pointing at server under test) by looking either for a `$environment_url` variable or for an
`environment_url.txt` file.

Therefore if an upstream job in the pipeline deployed your code to a server and propagated the deployed server url,
either through a [dotenv](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsdotenv) variable `$environment_url`
or through a basic `environment_url.txt` file, then the Puppeteer test will automatically be run on this server.

:warning: all our deployment templates implement this design. Therefore even purely dynamic environments (such as review
environments) will automatically be propagated to your Puppeteer tests.

If you're not using a smart deployment job, you may still explicitly declare the `PUPPETEER_BASE_URL` variable (but that
will be unfortunately hardcoded to a single server).

### Hook scripts

The Puppeteer template supports _optional_ **hook scripts** from your project, located in the `$PUPPETEER_PROJECT_DIR` directory to perform additional project-specific logic:

* `pre-puppeteer.sh` is executed **before** running Puppeteer,
* `post-puppeteer.sh` is executed **after** running Puppeteer (whichever the tests status).
