# [3.7.0](https://gitlab.com/to-be-continuous/puppeteer/compare/3.6.0...3.7.0) (2025-01-27)


### Features

* disable tracking service by default ([e340e22](https://gitlab.com/to-be-continuous/puppeteer/commit/e340e22dbab4d2252fce3a758a0b94835fa09cde))

# [3.6.0](https://gitlab.com/to-be-continuous/puppeteer/compare/3.5.0...3.6.0) (2024-08-30)


### Features

* standard TBC secrets decoding ([35ea7b1](https://gitlab.com/to-be-continuous/puppeteer/commit/35ea7b15eb3a65ed320c27336fe79089d10768c9))

# [3.5.0](https://gitlab.com/to-be-continuous/puppeteer/compare/3.4.1...3.5.0) (2024-05-06)


### Features

* add hook scripts support ([d1ac3c3](https://gitlab.com/to-be-continuous/puppeteer/commit/d1ac3c3d0ab13345ba9fec48618d4e25cf914bbb))

## [3.4.1](https://gitlab.com/to-be-continuous/puppeteer/compare/3.4.0...3.4.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([a464d31](https://gitlab.com/to-be-continuous/puppeteer/commit/a464d31ac8015cbce9b77b40d6a162648c13d71e))

# [3.4.0](https://gitlab.com/to-be-continuous/puppeteer/compare/3.3.0...3.4.0) (2024-1-27)


### Features

* migrate to CI/CD component ([eedfa87](https://gitlab.com/to-be-continuous/puppeteer/commit/eedfa8759f7c7e2b82258487dbbefac400096cb9))

# [3.3.0](https://gitlab.com/to-be-continuous/puppeteer/compare/3.2.1...3.3.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([1a727cf](https://gitlab.com/to-be-continuous/puppeteer/commit/1a727cfbbaba634999756db7701d71742625fbe6))

## [3.2.1](https://gitlab.com/to-be-continuous/puppeteer/compare/3.2.0...3.2.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([a22018a](https://gitlab.com/to-be-continuous/puppeteer/commit/a22018a07e85d885ac0993fe4d827f03bb914b51))

# [3.2.0](https://gitlab.com/to-be-continuous/puppeteer/compare/3.1.0...3.2.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([53ed089](https://gitlab.com/to-be-continuous/puppeteer/commit/53ed0892aec94e854c44267ed9e615dceb0d5537))

# [3.1.0](https://gitlab.com/to-be-continuous/puppeteer/compare/3.0.0...3.1.0) (2022-10-04)


### Features

* change default image ([a8a9e96](https://gitlab.com/to-be-continuous/puppeteer/commit/a8a9e96767eb054a2094f0738ad4df3c64761055))
* normalize reports ([179f97d](https://gitlab.com/to-be-continuous/puppeteer/commit/179f97dbec10e5a1c030f051b4f501f7c89185f3))

# [3.0.0](https://gitlab.com/to-be-continuous/puppeteer/compare/2.1.0...3.0.0) (2022-08-05)


### Features

* adapative pipeline ([b83a898](https://gitlab.com/to-be-continuous/puppeteer/commit/b83a898942be0ea68a4cb98ccd5b49d1f94a8768))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.1.0](https://gitlab.com/to-be-continuous/puppeteer/compare/2.0.1...2.1.0) (2022-05-01)


### Features

* configurable tracking image ([dcd4ff2](https://gitlab.com/to-be-continuous/puppeteer/commit/dcd4ff2b360b27180f2b105d919e79e52c8548b5))

## [2.0.1](https://gitlab.com/to-be-continuous/puppeteer/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([bd012bf](https://gitlab.com/to-be-continuous/puppeteer/commit/bd012bf5c1403f7a875cbdd33f7b22abac91931f))

## [2.0.0](https://gitlab.com/to-be-continuous/puppeteer/compare/1.2.0...2.0.0) (2021-09-08)

### Features

* Change boolean variable behaviour ([996418b](https://gitlab.com/to-be-continuous/puppeteer/commit/996418b81f9cecdf76d5f271f2797c103603db9a))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

Signed-off-by: Cédric OLIVIER <cedric3.olivier@orange.com>

## [1.2.0](https://gitlab.com/to-be-continuous/puppeteer/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([42bb2cb](https://gitlab.com/to-be-continuous/puppeteer/commit/42bb2cb168a080773e24722162a7317de10a2731))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/puppeteer/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([31ecc52](https://gitlab.com/Orange-OpenSource/tbc/puppeteer/commit/31ecc52f8c706cb1b9f44be3614626b1ced37090))

## 1.0.0 (2021-05-06)

### Features

* initial release ([6a17fa9](https://gitlab.com/Orange-OpenSource/tbc/puppeteer/commit/6a17fa96badb75c892e76521fbb82722a017a79a))
